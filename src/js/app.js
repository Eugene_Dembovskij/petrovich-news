// import $ from 'jquery';
import slick from 'slick';
import fancybox from 'fancybox';
import appear from 'appear';
import videojs from 'videojs';

const clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

const fncAnimation = (callback) => {
  window.setTimeout(callback, 1000 / 60);
  return callback;
};

window.requestAnimFrame = (() =>
  window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	fncAnimation
)();

function getScrollBarWidth() {
  const inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';

  const outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);

  document.body.appendChild(outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;
  if (w1 === w2) w2 = outer.clientWidth;

  document.body.removeChild(outer);
  return (w1 - w2);
}

(function(ELEMENT) {
  ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
  ELEMENT.closest = ELEMENT.closest || function closest(selector) {
    if (!this) return null;
    if (this.matches(selector)) return this;
    if (!this.parentElement) {return null;}
    else return this.parentElement.closest(selector);
  };
}(Element.prototype));

const Navbar = {
  body: null,
  html: null,
  btnToggle: null,
  overlay: null,
  init: function(element) {
    this.body = document.querySelector('body');
    this.html = document.querySelector('html');
    this.element = document.querySelector(element);

    if (this.element) {
      this.btnToggle = this.element.querySelector('[data-navbar-toggler]');
      this.overlay = document.querySelector('[data-navbar-overlay]');
      this.navbar = this.element.querySelector('.header__navbar');
      this.attachEvents();
      this.bind();
    }
  },
  attachEvents: function() {
    this.btnToggle.addEventListener(clickEventType, this.onClickToggle.bind(this));
    this.overlay.addEventListener(clickEventType, () => {
      if (this.body.classList.contains('mobile-nav-opened')) {
        this.closeMobileNav();
      }
    });
  },
  bind: function() {
    const self = this;
    window.addEventListener('resize', () => {
      if (window.innerWidth >= 993 && self.body.classList.contains('mobile-nav-opened')) {
        self.closeMobileNav();
      }
    });
    document.addEventListener('click', (ev) => {
      const test = !this.body.classList.contains('mobile-nav-opened') || ev.target.getAttribute('data-navbar-toggler') !== null;
      if (test) {
        return false;
      }
      const result = [this.navbar].filter((el) => {
        return (el && (el.contains(ev.target) || el === ev.target));
      });
      return (result.length === 0) ? this.closeMobileNav() : false;
    });
  },
  onClickToggle: function() {
    const header = document.querySelector('[data-header]');
    if (header.classList.contains('is-focus')) {
      Search.onClickClose();
    }
    if (this.body.classList.contains('mobile-nav-opened')) {
      this.closeMobileNav();
    } else {
      this.openMobileNav();
    }
  },
  openMobileNav: function() {
    const self = this;
    this.body.classList.add('mobile-nav-start');
    setTimeout(() => {
      self.btnToggle.classList.add('is-active');
      self.body.classList.add('mobile-nav-opened');
      self.lockScroll();
    }, 50);
  },
  closeMobileNav: function() {
    const self = this;
    this.btnToggle.classList.remove('is-active');
    this.body.classList.remove('mobile-nav-opened');
    setTimeout(() => {
      self.body.classList.remove('mobile-nav-start');
    }, 400);
    this.unlockScroll();
  },
  lockScroll: function() {
    const scrollPosition = [
      this.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
      this.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
    ];
    this.html.setAttribute('data-scroll-position', scrollPosition);
    this.html.setAttribute('data-previous-overflow', 'visible');
    this.html.style.overflow = 'hidden';
    window.scrollTo(scrollPosition[0], scrollPosition[1]);

    this.body.style.marginRight = `${getScrollBarWidth()}px`;
    this.html.classList.add('lock-scroll');
  },
  unlockScroll: function() {
    this.html.style.overflow = this.html.getAttribute('data-previous-overflow');
    const scrollPosition = this.html.getAttribute('data-scroll-position');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
    this.body.style.marginRight = 0;
    this.html.classList.remove('lock-scroll');
  }
};

const Search = {
  element: null,
  elHeader: null,
  elBtn: null,
  elBtnTarget: null,
  elClear: null,
  elInput: null,
  init: function(element) {
    this.element = document.querySelector(element);
    if (this.element) {
      this.elHeader = document.querySelector('[data-header]');
      this.elBtn = this.element.querySelector('[data-header-search-btn]');
      this.elBtnTarget = document.querySelector('[data-header-search-target]');
      this.elClear = this.element.querySelector('[data-header-search-clear]');
      this.elInput = this.element.querySelector('[data-header-search-input]');
      this.attachEvents();
      this.bind();
    }
  },
  attachEvents: function() {
    var self = this;
    this.elBtn.addEventListener('click', this.onClickOpen.bind(this));
    this.elBtnTarget.addEventListener('click', this.onClickOpen.bind(this));
    this.elClear.addEventListener('click', this.onClickClear.bind(this));
    window.addEventListener('resize', this.resize);
    this.elInput.addEventListener('focus', () => {
      self.elHeader.classList.add('is-focus');
    });
    // this.elInput.addEventListener('blur', (ev) => {
    //   if (ev.target.value === '' && self.elHeader.classList.contains('is-focus')) {
    //     self.elHeader.classList.remove('is-focus');
    //   }
    // });
    ['paste', 'keyup'].forEach((item) => {
      self.elInput.addEventListener(item, (ev) => {
        if (ev.target.value === '') {
          if (self.element.classList.contains('is-active')) {
            self.element.classList.remove('is-active');
          }
        } else {
          if (!self.element.classList.contains('is-active')) {
            self.element.classList.add('is-active');
          }
        }
      });
    });
  },
  bind: function() {
    const self = this;
    document.addEventListener('click', (ev) => {
      const test = self.elHeader.classList.contains('is-focus') && self.element.classList.contains('is-opened');
      if (test) {
        return false;
      }
      const result = [self.element].filter((el) => {
        return el && (el.contains(ev.target) || el === ev.target);
      });
      const resultTarget = [self.elBtnTarget].filter(function(el) {
        return el && (el.contains(ev.target) || el === ev.target);
      });
      return result.length === 0 && resultTarget.length === 0 ? self.onClickClose() : false;
    });
  },
  onClickBtn: function(ev) {
    if (this.elInput.value === '') {
      ev.preventDefault();
    }
  },
  onClickClose: function() {
    if (this.elHeader.classList.contains('is-focus')) {
      this.elHeader.classList.remove('is-focus');
    }
    if (this.elBtnTarget.classList.contains('is-active')) {
      this.elBtnTarget.classList.remove('is-active');
      if (this.element.classList.contains('is-opened')) {
        this.element.classList.remove('is-opened');
      }
    }
  },
  onClickClear: function() {
    // if (this.element.classList.contains('is-active')) {
    //   this.element.classList.remove('is-active');
    //   this.elInput.value = '';
    //   this.elInput.blur();
    //   this.elInput.focus();
    // }
    this.elInput.value = '';
    this.elInput.blur();
    this.onClickClose();
  },
  onClickOpen: function() {
    this.elHeader.classList.add('is-focus');
    if (!this.elBtnTarget.classList.contains('is-active')) {
      this.elBtnTarget.classList.add('is-active');
      if (!this.element.classList.contains('is-opened')) {
        this.element.classList.add('is-opened');
      }
    } else {
      this.onClickClose();
    }
  },
  resize: function() {}
};

let isMobile;

const windowScrollAnimations = function() {
  var $animate = $('.animate-up, .animate-down, .animate-left, .animate-right, .user-blockquote, .blockquote, .figure, .text-ins');

  $animate.appear();
  $animate.on('appear', function(event, affected) {
    for (var i = 0; i < affected.length; i++) {
      $(affected[i]).addClass('animated');
    }
  });
  $.force_appear();
};

const instSlider = {
  el: null,
  options: {
    responsive: {},
    sliderOptions: {
      dots: true,
      'slidesToShow': 5,
      'variableWidth': true,
      'infinite': false,
      'dots': false,
      'responsive': [
        {
          'breakpoint': 10000,
          'settings': {
            'slidesToShow': 5
          }
        },
        {
          'breakpoint': 960,
          'settings': {
            'slidesToShow': 4,
            'rows': 2
          }
        },
        {
          'breakpoint': 760,
          'settings': {
            'variableWidth': true,
            'slidesToShow': 2,
            'rows': 2
            // "adaptiveHeight": true
          }
        }
      ]
    }
  },
  create: function(element) {
    this.element = $(element);
    this.el = $(element);
    this.setLinks();
    this.bind();
    if (this.sliderElement.is(':visible') && this.destroyed !== false) {
      this.prepareOptions();
      this.init(this.afterInit.bind(this));
    }
  },
  bind: function() {
    window.addEventListener('resize', this.checkSlidesCount);
    // this.sliderElement.on('breakpoint', this.reInit.bind(this));
  },
  setLinks: function() {
    this.sliderElement = this.element.find('[data-inst-slider-list]');
    this.sliderControls = this.element.find('[data-inst-slider-controls]');
  },
  prepareOptions: function() {
    this.options.sliderOptions.appendArrows = this.sliderControls;

    const optionResponsive = this.options.sliderOptions.responsive;

    optionResponsive.map((item) => {
      this.options.responsive[item.breakpoint] = item.settings;
    });
  },
  init: function(callback) {
    this.destroyed = false;
    this.sliderElement.one('init', (event, slick) => {
      this.inited = true;
      if (callback && typeof callback === 'function') {
        callback(event, slick);
      }
    });

    this.sliderInst = this.sliderElement.slick(this.options.sliderOptions);
  },
  afterInit: function(event, element) {
    this.slick = element;
    this.checkSlidesCount();
  },
  destroy: function(callback) {
    this.inited = false;
    this.sliderElement.one('destroy', (event) => {
      this.destroyed = true;
      if (callback && typeof callback === 'function') {
        callback(event);
      }
    });

    this.sliderElement.slick('unslick');
  },
  reInit: function() {
    $(this.sliderControls).empty();
    if (this.sliderElement && this.inited) {
      this.destroy(() => {
        window.removeEventListener('resize', this.checkSlidesCount);
        this.sliderElement.off('breakpoint', this.reInit.bind(this));
        this.create(this.element);
        this.init();
      });
    }
  },
  checkSlidesCount: function() {
    if (this.slick) {
      const slideCount = this.slick.slideCount;
      const currentSettings = this.slick.breakpointSettings[this.slick.activeBreakpoint];

      if (typeof currentSettings === 'object' && typeof currentSettings.slidesToShow) {
        if (currentSettings.slidesToShow >= slideCount) {
          this.sliderControls.hide();
        } else {
          this.sliderControls.show();
        }
      }
    }
  }
};

const Slider = {
  create: function(element) {
    if ($(element).length) {
      $(element).each((ind, el) => {
        const slider = $(el).find('[data-inline-gallery-list]');
        slider.slick({
          'draggable': false,
          'arrow': (slider.children().length > 1) ? true : false,
          'slidesToShow': 1,
          'infinite': false,
          'dots': (slider.children().length > 1) ? true : false,
          'adaptiveHeight': true,
          'appendDots': $(el).find('[data-inline-gallery-controls]'),
          'appendArrows': $(el).find('[data-inline-gallery-controls]')
        });
      });
    }
  }
};

// fancybox init
const fancyboxGallery = {
  options: {
    baseClass: 'fancybox-gallery',
    infobar: false,
    buttons: ['close'],
    lang: 'ru',
    i18n: {
      ru: {
        CLOSE: 'Закрыть',
        NEXT: 'Вперед',
        PREV: 'Назад',
        ERROR: 'Не удалось загрузить медиаресурс. Пожалуйста, попробуйте позже',
        PLAY_START: 'Запустить слайдшоу',
        PLAY_STOP: 'Остановить слайдшоу',
        FULL_SCREEN: 'На весь экран',
        THUMBS: 'Миниатюры',
        DOWNLOAD: 'Скачать',
        SHARE: 'Поделиться',
        ZOOM: 'Увеличить',
      },
    },
    thumbs : {
      autoStart : true,
      axis: 'x'
    },
    btnTpl: {
      arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
      '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 68"><path class="st1" d="M34 68V0C11.333 3.064 0 14.398 0 34s11.333 30.936 34 34z" opacity=".5" transform="translate(-100 -4087) translate(100 4087) rotate(180 17 34)"/><path class="st2" d="M1.891 0L0.035 1.971 6.047 8.359 0.035 14.746 1.891 16.717 9.758 8.359z" transform="translate(-100 -4087) translate(100 4087) rotate(90 -4 22.5) rotate(90 8.5 8.5)"/></svg></div>' +
      '</button>',

      arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
      '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 68"><path class="st1" opacity=".5" d="M0 0c18.778 0 34 15.222 34 34S18.778 68 0 68z" transform="translate(-1446 -505) matrix(-1 0 0 1 1480 505)"/><path class="st2" d="M1.891 0L0.035 1.971 6.047 8.359 0.035 14.746 1.891 16.717 9.758 8.359z" transform="translate(-1446 -505) matrix(-1 0 0 1 1480 505) rotate(90 -4 22.5) rotate(90 8.5 8.5)"/></svg></div>' +
      '</button>',
    }
  },
  init: function() {
    if ($.fn.fancybox) {
      $('[data-inline-gallery]').each((i, gallery) => {
        $(gallery).find('img').each((j, img) => {
          const item = $(img);
          if (item.closest('a[href]').length) {
            item.closest('a[href]').attr('data-fancybox', `gallery-${i}`);
          } else {
            item.wrap(`<a class="inline-gallery__media" data-fancybox="gallery-${i}" href="${img.src}"></a>`);
          }
        });
      });
      $('[data-fancybox]').fancybox(this.options);
    }
  }
};

window.modal = {
  element: null,
  id: null,
  showTime: 500,
  hideTime: 500,
  beforeShowTime: 100,
  beforeHideTime: 100,
  zindex: 300,
  alert: false,
  init: function(element) {
    this.attachEvents(element);
  },
  attachEvents: function(element) {
    const arrayList = document.querySelectorAll(element);
    const listModals = document.querySelectorAll('[data-modal-open]');
    Array.prototype.forEach.call(listModals, (el) => {
      if (el.getAttribute('data-modal-open') !== null) {
        this.id = el.getAttribute('data-modal-open');
        el.addEventListener('click', () => {
          Array.prototype.forEach.call(arrayList, (item) => {
            if (item.getAttribute('data-modal') === this.id) {
              this.element = item;
              this.wrapperElement = item.querySelector('[data-modal-wrapper]');
              this.btnClose = item.querySelector('[data-modal-close]');
              this.backdrop = item.querySelector('[data-modal-backdrop]');
              if (this.btnClose) {
                this.btnClose.addEventListener('click', () => {
                  this.close();
                  setTimeout(() => {
                    this.btnClose.removeEventListener('click', () => {});
                  }, this.beforeHideTime);
                });
              }
              if (this.backdrop) {
                this.backdrop.addEventListener('click', () => {
                  this.close();
                });
              }
            }
          });
          this.open();
        });
      }
    });
  },
  bind: function() {
    document.addEventListener('click', (ev) => {
      const test = !this.element.classList.contains('show');
      if (test) {
        return false;
      }
      const result = [this.element].filter((el) => {
        return (el && (el.contains(ev.target) || el === ev.target));
      });
      return (result.length === 0) ? this.close() : false;
    });
  },
  close: function() {
    const html = document.querySelector('html');
    if (!this.element.classList.contains('beforehide')) {
      this.element.classList.add('beforehide');
    }
    setTimeout(() => {
      if (this.element.classList.contains('show')) {
        this.element.classList.remove('show');
      }
      if (this.element.classList.contains('beforehide')) {
        this.element.classList.remove('beforehide');
      }
      if (!this.element.classList.contains('hide')) {
        this.element.classList.add('hide');
      }
      setTimeout(() => {
        this.element.style.display = 'none';
        this.element.style.zIndex = 0;
        if (html.classList.contains('lock-scroll')) {
          html.classList.remove('lock-scroll');
        }
        if (this.element.classList.contains('hide')) {
          this.element.classList.remove('hide');
        }
      }, this.beforeHideTime);
    }, this.beforeHideTime);
  },
  open: function() {
    const html = document.querySelector('html');

    if (!html.classList.contains('lock-scroll')) {
      html.classList.add('lock-scroll');
    }
    if (!this.element.classList.contains('hide')) {
      this.element.classList.remove('hide');
    }
    if (!this.element.classList.contains('beforeshow')) {
      this.element.classList.add('beforeshow');
    }
    this.element.style.cssText = `z-index:${this.getZIndex()};display:block;`;
    setTimeout(() => {
      if (this.element.classList.contains('beforeshow')) {
        this.element.classList.remove('beforeshow');
      }
      if (!this.element.classList.contains('show')) {
        this.element.classList.add('show');
      }
    }, this.beforeShowTime);

  },
  showModal: function(id) {
    const arrayList = document.querySelectorAll('[data-modal]');
    Array.prototype.forEach.call(arrayList, (item) => {
      if (item.getAttribute('data-modal') === id) {
        this.element = item;
        this.wrapperElement = item.querySelector('[data-modal-wrapper]');
        this.btnClose = item.querySelector('[data-modal-close]');
        this.backdrop = item.querySelector('[data-modal-backdrop]');
        if (this.btnClose) {
          this.btnClose.addEventListener('click', () => {
            this.close();
            setTimeout(() => {
              this.btnClose.removeEventListener('click', () => {});
            }, this.beforeHideTime);
          });
        }
        if (this.backdrop) {
          this.backdrop.addEventListener('click', () => {
            this.close();
          });
        }
        this.open();
      }
    });
  },
  closeModal: function(id) {
    const arrayList = document.querySelectorAll('[data-modal]');
    Array.prototype.forEach.call(arrayList, (item) => {
      if (item.getAttribute('data-modal') === id) {
        this.element = item;
        this.wrapperElement = item.querySelector('[data-modal-wrapper]');
        this.close();
      }
    });
  },
  getZIndex: function() {
    this.zIndexCounter = this.zIndexCounter || 0;
    this.zIndexCounter++;
    return parseFloat(this.zindex) + this.zIndexCounter;
  }
};

const Anchor = {
  element: null,
  offsetTop: 0,
  init: function(el) {
    this.element = document.querySelector(el);
    this.offsetTop = window.innerHeight * 2;
    if (this.element) {
      window.addEventListener('scroll', this.checkVisibility.bind(this));
      window.addEventListener('resize', this.resizeHandler.bind(this));
      this.element.addEventListener('click', this.onClickHandle.bind(this));
    }
  },
  checkVisibility: function() {
    if (window.pageYOffset > this.offsetTop) {
      if (!this.element.classList.contains('show')) {
        this.element.classList.add('show');
      }
    } else {
      if (this.element.classList.contains('show')) {
        this.element.classList.remove('show');
      }
    }
  },
  resizeHandler: function() {
    this.offsetTop = window.innerHeight * 2;
  },
  onClickHandle: function(e) {
    e.preventDefault();
    const offsetTop = e.currentTarget.getAttribute('data-offset') || 0;
    this.scrollToY(offsetTop);
  },
  scrollToY(targetY) {
    const scrollTargetY = targetY;
    const scrollY = window.scrollY || document.documentElement.scrollTop;
    let currentTime = 0;
    const time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / 2000, 0.8));

    const easingEquations = {
      easeOutSine(pos) {
        return Math.sin(pos * (Math.PI / 2));
      }
    };

    const tick = () => {
      currentTime += 1 / 60;
      const p = currentTime / time;
      const t = easingEquations['easeOutSine'](p);

      if (p < 1) {
        window.requestAnimFrame(tick);
        window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
      } else {
        window.scrollTo(0, scrollTargetY);
      }
    };

    tick();
  }
};

const counterSlider = {
  create: function(element) {
    if ($(element).length) {
      $(element).each((ind, el) => {
        const slider = $(el).find('[data-counter-gallery-list]');
        if (slider.children().length > 1) {
          slider.slick({
            'draggable': false,
            'arrow': true,
            'slidesToShow': 1,
            'infinite': false,
            'dots': true,
            'adaptiveHeight': true,
            'appendArrows': $(el).find('[data-counter-gallery-controls]')
          });
        }
      });
    }
  }
};


// // anchor smooth scroll
// $(document).on('click', '[data-anchor]', function(e) {
//   e.preventDefault();

//   const $this = $(this);
//   let topOffset = $this.data('offset') || 0;
//   let target = $this.data('anchor') || $this.attr('href');
//   target = $(target);
//   if (target.length) {
//     $('html, body').animate({
//       scrollTop: target.offset().top + topOffset
//     }, 300);
//   }
// });

const musicPlayer = {
  globalMusicPlayer: [],
  element: null,
  globalPlayer: '',
  init: function(el) {
    const elementList = document.querySelectorAll(el);
    if (elementList.length) {
      for (let i = 0; i < elementList.length; i++) {
        this.element = elementList[i];
        const btn = this.element.querySelector('[data-music-player-control]');
        btn.addEventListener('click', this.onClickPlay.bind(this));
      }
    }
  },
  getOptions(pathAudio) {
    return {
      controls: true,
      autoplay: false,
      preload: 'auto',
      children: ['mediaLoader', 'loadingSpinner', 'controlBar', 'errorDisplay'],
      controlBar: {
        playToggle: false,
        volumePanel: false,
        descriptionsButton: false,
        subsCapsButton: false,
        fullscreenToggle: false,
        timeDivider: false,
        remainingTimeDisplay: false
      },
      height: 20,
      // muted: true,
      playsinline: 'playsinline',
      sources: [
        {
          src: pathAudio,
          type: 'audio/mp3'
        }
      ],
      fill: true,
      fluid: false,
      responsive: false
    };
  },
  onClickPlay: function(event) {
    const self = this;
    const currentTarget = event.currentTarget;
    const parent = currentTarget.closest('[data-music-player]');

    var curId = parent.querySelector('[data-music-player-element]').children[0].getAttribute('id');
    function initPlayer() {
      self.globalPlayer = videojs(curId, self.getOptions(currentTarget.getAttribute('data-src')), () => {
        self.globalPlayer.el_.classList.add('video-js');
        self.globalPlayer.volume(0.1);
        const parent = currentTarget.closest('[data-music-player]');
        let container = parent.querySelector('[data-music-player-element]');
        container.style.display = 'block';
      });
      self.globalPlayer.on('ended', function() {
        this.dispose();
        currentTarget.classList.remove('is-active');
        const parent = currentTarget.closest('[data-music-player]');
        let container = parent.querySelector('[data-music-player-element]');
        container.style.display = 'none';
        container.innerHTML = '<audio id="' + curId + '"></audio>';
        self.globalMusicPlayer.forEach((item, index, obj) => {
          item.dispose();
          obj.splice(index, 1);
        });
        self.globalPlayer = '';
      });
      self.globalMusicPlayer.push(self.globalPlayer);
    }
    function removeIsAcitve() {
      const controlList = document.querySelectorAll('.is-active[data-music-player-control]');
      for (let i = 0; i < controlList.length; i++) {
        const item = controlList[i];
        item.classList.remove('is-active');
        item.classList.remove('is-init');
        const parent = item.closest('[data-music-player]');
        let player = parent.querySelector('.video-js');
        var container = player.parentNode;
        if (player.classList.contains('video-js')) {
          self.globalMusicPlayer.forEach((item, index, obj) => {
            item.dispose();
            obj.splice(index, 1);
          });
          self.globalPlayer = '';
          player.classList.remove('video-js');
          container.innerHTML = '<audio id="' + player.getAttribute('id') + '"></audio>';
          container.style.display = 'none';
        }
      }
    }
    function removeIsInit() {
      const controlList = document.querySelectorAll('.is-init[data-music-player-control]');
      for (let i = 0; i < controlList.length; i++) {
        const item = controlList[i];
        item.classList.remove('is-init');
        const parent = item.closest('[data-music-player]');
        let player = parent.querySelector('.video-js');
        var container = player.parentNode;
        if (player.classList.contains('video-js')) {
          self.globalMusicPlayer.forEach((item, index, obj) => {
            item.dispose();
            obj.splice(index, 1);
          });
          self.globalPlayer = '';
          player.classList.remove('video-js');
          container.innerHTML = '<audio id="' + player.getAttribute('id') + '"></audio>';
          container.style.display = 'none';
        }
      }
    }

    if (!currentTarget.classList.contains('is-active')) {
      removeIsAcitve();
      currentTarget.classList.add('is-active');
      if (self.globalMusicPlayer.length > 0) {
        if (currentTarget.classList.contains('is-init')) {
          let container = parent.querySelector('[data-music-player-element]');
          container.style.display = 'block';
          self.globalPlayer.play();
        } else {
          removeIsInit();
          initPlayer();
          self.globalPlayer.on('ready', function() {
            setTimeout(() => {
              this.play();
              currentTarget.classList.add('is-init');
            }, 0);
          });
        }
      } else {
        initPlayer();
        self.globalPlayer.on('ready', function() {
          setTimeout(() => {
            this.play();
            currentTarget.classList.add('is-init');
          }, 0);
        });
      }
    } else {
      currentTarget.classList.remove('is-active');
      self.globalPlayer.pause();
    }
  }
};

document.addEventListener('DOMContentLoaded', function() {
  Navbar.init('[data-header]');
  Search.init('[data-header-search]');
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isMobile = true;
    document.querySelector('html').classList.add('mobile');
  } else {
    isMobile = false;
    document.querySelector('html').classList.add('desktop');
  }
  windowScrollAnimations();
  Slider.create('[data-inline-gallery]');
  instSlider.create('[data-inst-slider]');
  fancyboxGallery.init();
  window.modal.init('[data-modal]');
  Anchor.init('[data-anchor]');
  counterSlider.create('[data-counter-gallery]');
  musicPlayer.init('[data-music-player]');
});
