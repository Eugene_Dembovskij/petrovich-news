// show more init
const showMore = {
  init: function() {
    $('[data-load-more]:not(.init)').each((i, el) => {
      const $this = $(el);
      const $btn = $this.find('.btn');
      const action = $this.data('action') || location.pathname;
      const maxPage = $this.data('max');
      const pagen = $this.data('pagen');
      const componentId = $this.data('component');
      const targetSelector = $this.data('target');
      let curPage = 1;
      let $target;

      if (typeof maxPage === 'undefined') {
        return false;
      }

      if (targetSelector) {
        $target = $(targetSelector);
      } else {
        $target = $this.prev('[data-load-more-content]');
      }

      $btn.on('click', e => {
        e.preventDefault();

        curPage += 1;
        if (curPage <= maxPage) {
          $btn.attr('disabled', 'disabled').addClass('loading');
          let url = `${action}?PAGEN_${pagen}=${curPage}`;
          if (componentId) {
            url += `&componentId=${componentId}`;
          }
          $.get(url, (res) => {
            setTimeout(() => {
              $target.append(res);

              if (curPage === maxPage) {
                $this.remove();
              } else {
                $btn.removeClass('loading').removeAttr('disabled');
              }
            }, 2000);
          });
        }
      });
      $this.addClass('init');
    });
  }
};

document.addEventListener('DOMContentLoaded', function() {
  showMore.init();
})
